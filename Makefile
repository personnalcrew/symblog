# Variables
DOCKER = docker
DOCKER_COMPOSE = docker-compose
EXEC = $(DOCKER) exec -w /var/www/project www_symblog
PHP = $(EXEC) php
COMPOSER = $(EXEC) composer
NPM = $(EXEC) npm
SYMFONY_CONSOLE = $(EXEC) bin/console

# Colors
GREEN = /bin/echo -e "\x1b[32m\#\# $1\x1b[0m"
RED = /bin/echo -e "\x1b[31m\#\# $1\x1b[0m"

## -- App --
init: ## Init the project
	$(MAKE) docker-start
	$(MAKE) composer-install
	@$(call GREEN, "The application is available at: http://127.0.0.1:8000/.")

cache-clear: ## clear cache
	$(SYMFONY_CONSOLE) cache:clear

## -- Docker --
docker-start: ## Start App
	$(DOCKER_COMPOSE) up -d

## -- Composer --
composer-install: ## Install Dependencies
	$(COMPOSER) install

composer-update: ## update Dependencies
	$(COMPOSER) update

## -- Database --
database-init: ## Init database
	$(MAKE) database-drop
	$(MAKE) database-create
	$(MAKE) database-migrate
	$(MAKE) fixtures

database-drop: ## Drop database
	$(SYMFONY_CONSOLE) d:d:d --force --if-exists

database-create: ## Create database
	$(SYMFONY_CONSOLE) d:d:c --force --if-exists

database-migration: ## Make migration
	$(SYMFONY_CONSOLE) d:m:m --no-interaction

migrate: ## Alias : database-migrate
	$(MAKE) database-migrate

database-fixture-load: ## Load fixture
	$(SYMFONY_CONSOLE) d:f:l -- no-interacations

fixtures:
	$(MAKE) database-fixtures-load